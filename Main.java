import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Какую математическую операцию вы хотите использовать ? ");
        String operation = in.nextLine();
        System.out.print("Введите число1: ");
        double number1 = in.nextDouble();
        System.out.print("Введите число2: ");
        double number2 = in.nextDouble();

        double result = 0;
        String error = " ";
        switch (operation) {
            case "/":
                result = number1 / number2;
                System.out.println("Ответ: " + result);
                break;
            case "*":
                result = number1 * number2;
                System.out.println("Ответ: " + result);
                break;
            case "+":
                result = number1 + number2;
                System.out.println("Ответ: " + result);
                break;
            case "-":
                result = number1 - number2;
                System.out.println("Ответ: " + result);
                break;
            default:
                error = "Нет такой математической операции";
                System.out.println(error);
                break;
        }
    }
}